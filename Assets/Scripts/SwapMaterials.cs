using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapMaterials : MonoBehaviour
{
    private Material defaultMaterial;

    public Material highlightedMaterial;
    // Start is called before the first frame update
    void Start()
    {
        defaultMaterial = GetComponent<MeshRenderer>().material;
    }

    private void OnMouseOver()
    {
        GetComponent<MeshRenderer>().material = highlightedMaterial;
    }

    private void OnMouseExit()
    {
        GetComponent<MeshRenderer>().material = defaultMaterial;
    }

   
}
